DBLOG QUICK FILTER
===============

Description
===============
Redo the Dblog Filter page using Angular js based filtering.

Installation
===============
Create a directory within modules/custom/dblog_quick_filter/js/ named [angular]
Locate/download/extract the Angularjs 1.6.2 [https://code.angularjs.org/1.6.2/angular-1.6.2.zip] contents to the [angular] folder

Requirements
============
* angular-1.6.2.min.js

Authors/maintainers
===================
